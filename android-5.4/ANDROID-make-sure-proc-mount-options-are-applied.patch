From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: Todd Kjos <tkjos@google.com>
Date: Fri, 6 Dec 2019 09:35:24 -0800
Subject: ANDROID: make sure proc mount options are applied

Android relies on the "hidepid" mount option for /proc on
the 2nd mount, however the upstream kernel requires options
on the first mount and ignores options afterwards.

In prevous Android kernels, this was fixed by reverting
upstream commit e94591d0d90c ("proc: Convert proc_mount
to use mount_ns."). The upstream code has now been
refactored to the point that a new fix is needed. This
patch applies mount options during proc_get_tree() to
ensure the most recently parsed options are applied.

Bug: 145626724
Test: atest CtsOsTestCases:android.os.cts.EnvironmentTest#testHidePid2
Change-Id: I3d402f98e826e2f03ad366da7d05b3eeaaa90c26
Signed-off-by: Todd Kjos <tkjos@google.com>
---
 fs/proc/root.c | 8 ++++----
 1 file changed, 4 insertions(+), 4 deletions(-)

diff --git a/fs/proc/root.c b/fs/proc/root.c
index 485237e7cce1..e277722f443b 100644
--- a/fs/proc/root.c
+++ b/fs/proc/root.c
@@ -82,8 +82,7 @@ static int proc_parse_param(struct fs_context *fc, struct fs_parameter *param)
 	return 0;
 }
 
-static void proc_apply_options(struct super_block *s,
-			       struct fs_context *fc,
+static void proc_apply_options(struct fs_context *fc,
 			       struct pid_namespace *pid_ns,
 			       struct user_namespace *user_ns)
 {
@@ -101,7 +100,7 @@ static int proc_fill_super(struct super_block *s, struct fs_context *fc)
 	struct inode *root_inode;
 	int ret;
 
-	proc_apply_options(s, fc, pid_ns, current_user_ns());
+	proc_apply_options(fc, pid_ns, current_user_ns());
 
 	/* User space would break if executables or devices appear on proc */
 	s->s_iflags |= SB_I_USERNS_VISIBLE | SB_I_NOEXEC | SB_I_NODEV;
@@ -149,7 +148,7 @@ static int proc_reconfigure(struct fs_context *fc)
 
 	sync_filesystem(sb);
 
-	proc_apply_options(sb, fc, pid, current_user_ns());
+	proc_apply_options(fc, pid, current_user_ns());
 	return 0;
 }
 
@@ -157,6 +156,7 @@ static int proc_get_tree(struct fs_context *fc)
 {
 	struct proc_fs_context *ctx = fc->fs_private;
 
+	proc_apply_options(fc, ctx->pid_ns, current_user_ns());
 	return get_tree_keyed(fc, proc_fill_super, ctx->pid_ns);
 }
 
